import Postgres from "../database/postgres";
import { db } from "../database";
import { ISettings } from "@/models/settings";
import { Repo } from "@/interfaces/Repo";
import { DBModelService } from "./dbmodel.service";

export interface ISettingsService extends Repo<ISettings> {}

export class SettingsService extends DBModelService<ISettings> implements ISettingsService {
    // TODO Postgres should not be used here, instead the type used here should be generic.
    db: Postgres;
    constructor() {
        super(db, "hmi_settings");
        this.db = db;
    }

    /**
     * 
     * @param key 
     * @returns 
     */
    public get = async (...conditions: {[key: string]: any}[]): Promise<ISettings> => {
        let response = this.getByConditions(...conditions);

        return response[0] as Promise<ISettings>;
    }

    /**
     * 
     * @param conditions 
     * @returns 
     */
    public getAll = async (...conditions: {[key: string]: any}[]): Promise<ISettings[]> => {
        return this.getByConditions(...conditions) as Promise<ISettings[]>;
    }

    /**
     * 
     * @param setting 
     * @returns 
     */
    public create = async (setting: Partial<ISettings>): Promise<ISettings> => {
        await this.db.query(
            `INSERT INTO ${this.relation} (key, value, type, defaultValue) VALUES ($1,$2,$3,$4)`,
            [setting.key, setting.value, setting.type, setting.defaultValue]
        )
        return this.get(setting.key) as Promise<ISettings>;
    }

    /**
     * 
     * @param conditions 
     * @param setters 
     * @returns 
     */
    public update = async (conditions: { [key: string]: any }[], setters: { [key: string]: any }[]): Promise<ISettings> => {
        await this.enhancedUpdate(conditions, setters);
        return this.get({"key": setters["key"]}) as Promise<ISettings>;
    }

    /**
     * 
     * @param id 
     * @returns 
     */
     public delete = async (id: String) : Promise<boolean> => {
        throw new Error("Not yet implemented");
    }
}