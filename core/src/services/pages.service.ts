import Postgres from "../database/postgres";
import { db } from "../database";
import { IPage } from "@/models/pages";
import { v4 as uuidv4 } from 'uuid';
import { Repo } from "@/interfaces/Repo";
import { DBModelService } from "./dbmodel.service";

export interface IPagesService extends Repo<IPage> {}

export class PagesService extends DBModelService<IPage> implements IPagesService  {
    // TODO Postgres should not be used here, instead the type used here should be generic.
    db: Postgres;
    constructor() {
        super(db, "pages");
        this.db = db;
    }

    /**
     * 
     * @param id 
     * @returns 
     */
    public get = async (...conditions: {[key: string]: any}[]) : Promise<IPage> => {
        let response = this.getByConditions(...conditions);
        console.log("pages.service.ts ~ line 21 ~ PagesService ~ get= ~ response ", response);

        return response[0] as Promise<IPage>;
    }

    /**
     * 
     * @param conditions 
     * @returns 
     */
    public getAll = async (...conditions: {[key: string]: any}[]) : Promise<IPage[]> => {
        const response = this.getByConditions(...conditions) as Promise<IPage[]>;
        console.log("pages.service.ts ~ line 29 ~ PagesService ~ getAll= ~ response ", response);

        return response;
    }

    /**
     * 
     * @param page 
     * @returns 
     */
    public create = async (page: Partial<IPage>) : Promise<IPage> => {
        const id = uuidv4()
        await this.db.query(
            `INSERT INTO ${this.relation} (id, name, url, content, icon, menu_position) VALUES ($1,$2,$3,$4,$5,$6)`,
            [page.id, page.name, page.url, page.content, page.icon, page.menuPosition]
        );

        return this.get(page.id);
    }

    /**
     * 
     * @param conditions 
     * @param setters 
     * @returns 
     */
    public update = async (conditions: { [key: string]: any }[], setters: { [key: string]: any }[]) : Promise<IPage> => {
        await this.enhancedUpdate(conditions, setters);
        return this.get({"id": setters["id"]});
    }

    /**
     * 
     * @param id 
     * @returns 
     */
    public delete = async (id: String) : Promise<boolean> => {
        throw new Error("Not yet implemented");
    }
}