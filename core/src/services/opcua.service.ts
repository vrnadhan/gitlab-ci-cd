import Postgres from "../database/postgres";
import { db } from "../database";

export interface IOPCUAService {
    get(): Promise<any>;
    getAll(): Promise<any[]>;
    create(node: Partial<any>): Promise<any>;
    update(node: Partial<any>): Promise<any>;
}

export class OPCUAService implements IOPCUAService {
    // TODO Postgres should not be used here, instead the type used here should be generic.
    db: Postgres;
    constructor() {
        this.db = db;
    }

    public get = async () : Promise<any> => {
        throw new Error("Not yet implemented");
    }

    public getAll = async () : Promise<any[]> => {
        throw new Error("Not yet implemented");
    }

    public create = async (node: Partial<any>) : Promise<any> => {
        throw new Error("Not yet implemented");
    }

    public update = async (node: Partial<any>) : Promise<any> => {
        throw new Error("Not yet implemented");
    }
}