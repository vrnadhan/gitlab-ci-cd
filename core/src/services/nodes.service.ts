import Postgres from "../database/postgres";
import { db } from "../database";
import { INode } from "../models/nodes";
import { DBModelService } from "./dbmodel.service";

export interface INodesService {
    // Get a single entity that matches with node passed as an argument
    get(...conditions: {[key: string]: any}[]): Promise<INode>;
    // Get all nodes in the DB
    getAll(...conditions: {[key: string]: any}[]): Promise<INode[]>;
    // Create a new node
    create(node: Partial<INode>): Promise<INode>;
    // Search for a node by a key (searchByKey) and update it with the values in updatedEntity
    update(searchByKey: any, updatedEntity: Partial<INode>): Promise<INode>;
    // Delete a node that matches with the node passed as an argument
    delete(node: any): Promise<boolean>;
}

export class NodesService extends DBModelService<INode> implements INodesService {
    // TODO Postgres should not be used here, instead the type used here should be generic.
    db: Postgres;
    constructor() {
        super(db, "nodes");
        this.db = db;
    }

    /**
     * 
     * @param node 
     * @returns 
     */
    public get = async (...conditions: {[key: string]: any}[]): Promise<INode> => {
        let response = this.getByConditions(...conditions);

        return response[0] as Promise<INode>;
    }

    /**
     * 
     * @param conditions 
     * @returns 
     */
    public getAll = async (...conditions: {[key: string]: any}[]): Promise<INode[]> => this.getByConditions(...conditions) as Promise<INode[]>;

    /**
     * 
     * @param node 
     * @returns 
     */
    public create = async (node: Partial<INode>): Promise<INode> => {
        await this.db.query(`INSERT INTO ${this.relation} (path, subscribe) VALUES ($1,$2)`, [node.path, node.subscribe])

        return this.get(node);
    }

    /**
     * 
     * @param searchByKey 
     * @param updatedEntity 
     * @returns 
     */
    public update = async (searchByKey: any, updatedEntity: Partial<INode>): Promise<INode> => {
        await db.query(
            `UPDATE ${this.relation} SET subscribe = ${updatedEntity.subscribe} where node = '${searchByKey}'`,
            []
        );

        return this.get(searchByKey);
    }

    /**
     * 
     * @param node 
     * @returns 
     */
    public delete = async (node: any): Promise<boolean> => {
        throw new Error("Not yet implemented");
    }
}