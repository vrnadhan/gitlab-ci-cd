import Postgres from "@/database/postgres";

export class DBModelService<Relation> {
    db: Postgres;
    relation: string;
    constructor(db: Postgres, relation: string) {
        this.db = db;
        this.relation = relation;
    }

    /**
    * 
    * @param conditions 
    * @returns 
    */
    public getByConditions = async (...conditions: { [key: string]: any }[]): Promise<Relation[]> => {
        let query_parts = [`SELECT * from ${this.relation}`];
        const keys = conditions.map(c => Object.keys(c)[0]);
        if(keys.length > 0) {
            query_parts.push('WHERE');

            conditions.forEach((c, i) => {
                // TODO What about other equality operators?
                query_parts.push(`${keys[i]} = ${c[keys[i]]}`)
                if (i !== conditions.length - 1) {
                    // TODO What about other connectives?
                    query_parts.push('AND')
                }
            });
        }
        console.log("file: dbmodel.service.ts ~ line 18 ~ DBModelService<Relation> ~ getByConditions= ~ Query:", query_parts)
        return this.db.query(query_parts.join(" "), []);
    }

    public enhancedUpdate = async (conditions: { [key: string]: any }[], setters: { [key: string]: any }[]): Promise<Relation[]> => {
        if(setters.length === 0)
            return Promise.reject(new Error("Incorrect UPDATE statement!"));

        let query_parts = [`UPDATE ${this.relation}`];
        // Prepare the conditonal parts of the query
        const ckeys = conditions.map(c => Object.keys(c)[0]);
        if(ckeys.length > 0) {
            query_parts.push('WHERE');

            conditions.forEach((c, i) => {
                // TODO What about other equality operators?
                query_parts.push(`${ckeys[i]} = ${c[ckeys[i]]}`)

                if (i !== conditions.length - 1) {
                    // TODO What about other connectives?
                    query_parts.push('AND')
                }
            });
        }

        query_parts.push(`SET`);
        // Prepare the setter parts of the query
        const skeys = setters.map(c => Object.keys(c)[0]);
        setters.forEach((setter, i) => {
            query_parts.push(`${skeys[i]} = ${setter[skeys[i]]}`)

            if(i !== setters.length - 1) {
                query_parts.push(',')
            }
        });

        return this.db.query(query_parts.join(" "), []);
    }
}
