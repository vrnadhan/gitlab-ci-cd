import { Pool } from 'pg'
import { DB_HOST, DB_NAME, DB_USER, DB_PASSWORD } from '../env.js'

export default class {
  pool: Pool

  constructor() {
    this.pool = new Pool({
      user: DB_USER,
      host: DB_HOST,
      database: DB_NAME,
      password: DB_PASSWORD,
    })
  }

  async query(query: string, params: any) {
    try {
      const { rows } = await this.pool.query(query, params)
      return rows
    } catch (e) {
      if (
        e.code === '42P07' ||
        e.code === '42P16' ||
        e.code === '42701' ||
        e.code === '42704'
      ) {
        // Postgres thorws error 42P07 if table already exists which is ok.
        return
      }
      if (e.code === '23505') {
        return
      }
      if (e.code === '23505') {
        throw new Error('Duplicate key value violates unique constraint')
      }
      console.error(e)
      throw e
    }
  }
}
