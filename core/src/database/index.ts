
import Postgres from './postgres';
import env from '../env.js';

console.log('INFO:: Initializing Postgres schema', env.DB_NAME);
const db = new Postgres();

// Initalize the relations
(async function() {
    console.log('INFO:: Creating relations in schema', env.DB_NAME);
    // TODO Relations might already exist. Should we still attempt CREATE?
    // TODO Prepare a dynamic create table query based on the model definition we have here?
    // WARNING Migrations not available
    
    // settings
	try {
        await db.query(
            'CREATE TABLE hmi_settings (key varchar UNIQUE, value json, type varchar, defaultValue json)',
            []
        );
    } catch (e) {
        console.error(e);
    }
    // pages
    try {
        await db.query(
            'CREATE TABLE pages (id uuid, name varchar, url varchar, content json, icon varchar, menu_position int)',
            []
        );
    } catch (e) {
        console.error(e);
    }
    // nodes
    try {
        await db.query(
            'CREATE TABLE nodes (path varchar, subscribe boolean)',
            []
        );
    } catch (e) {
        console.error(e);
    }

    console.log('INFO:: Initialized relations hmi_settings, pages, nodes');
})();

export {
    db
}
