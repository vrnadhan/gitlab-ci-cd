import OPCValue from '@hmi/types/OPCValue'
import AchievedLevel from './models/AchievedLevel'
import LineLaserLevel from './models/LineLaserLevel'
import EventBus from './models/EventBus'

export default class Store {
  OPCState: any[]
  achievedLevel: AchievedLevel
  lineLaserLevel: LineLaserLevel
  heartbeat: NodeJS.Timeout

  constructor() {
    this.OPCState = []
    this.achievedLevel = new AchievedLevel()
    this.lineLaserLevel = new LineLaserLevel()

    EventBus.on(TramEvent.OPC_VALUE_CHANGED.toString(), (data) => {
      this.update(data)
    })

    EventBus.on(TramEvent.NEW_WEBSOCKET_CLIENT.toString(), (ws) => {
      this.sendStateToClient(ws)
    })

    EventBus.on(
      TramEvent.MESSAGE_FROM_WEBSOCKET_CLIENT.toString(),
      ({ key, value }) => {
        const { dataType } = this.OPCState[key]
        EventBus.emit(TramEvent.OPC_WRITE_VALUE.toString(), {
          key,
          value,
          dataType,
        } as OPCValue)
      }
    )

    this.startHeartBeat()
  }

  startHeartBeat() {
    this.heartbeat = setInterval(() => {
      if (this.OPCState['HMI_HeartBeat']) {
        EventBus.emit(TramEvent.OPC_WRITE_VALUE.toString(), {
          ...this.OPCState['HMI_HeartBeat'],
          value: !this.OPCState['HMI_HeartBeat'].value,
        } as OPCValue)
      }
    }, 1000)
  }

  sendStateToClient(ws: WebSocket) {
    for (const key of Object.keys(this.OPCState)) {
      EventBus.emit(TramEvent.SEND_TO_CLIENT.toString(), {
        ws,
        msg: {
          type: 'OPC',
          key,
          value: this.OPCState[key].value,
          dataType: this.OPCState[key].dataType,
        },
      })
    }
    EventBus.emit(TramEvent.SEND_TO_CLIENT.toString(), {
      ws,
      msg: {
        type: 'LineLaserGraph',
        list: this.lineLaserLevel.list,
      },
    })
    EventBus.emit(TramEvent.SEND_TO_CLIENT.toString(), {
      ws,
      msg: {
        type: 'AchievedLevel',
        list: this.achievedLevel.list,
      },
    })
  }

  update(data: OPCValue) {
    if (data.key === 'Pattern_current.pour_time') {
      this.achievedLevel.setPourTime(data.value)
    }
    if (data.key === 'Par.SENS_top_off_mold') {
      this.achievedLevel.setTopOfMold(data.value)
    }
    if (data.key === 'Par.SENS_level_scale') {
      this.achievedLevel.setLevelScale(data.value)
    }
    if (data.key === 'Par.SENS_nominal_value') {
      this.achievedLevel.setNominalValue(data.value)
    }
    if (data.key === 'achieved_level') {
      this.achievedLevel.addValue(data.value)
      EventBus.emit(TramEvent.SEND_TO_CLIENTS.toString(), {
        type: 'AchievedLevel',
        list: this.achievedLevel.list,
      })
      return
    }
    if (data.key === 'SENS_Level_avg') {
      this.lineLaserLevel.addValue(data.value)
      EventBus.emit(TramEvent.SEND_TO_CLIENTS.toString(), {
        type: 'LineLaserGraph',
        list: this.lineLaserLevel.list,
      })
      return
    }

    this.OPCState[data.key] = data
    EventBus.emit(TramEvent.SEND_TO_CLIENTS.toString(), {
      type: 'OPC',
      ...data,
    })
  }
}
