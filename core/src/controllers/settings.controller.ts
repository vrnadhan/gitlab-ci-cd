import { ISettings } from '../models/settings';
import {ISettingsService} from '../services/settings.service';

export default class {
  service: ISettingsService;
  constructor(service: ISettingsService) {
    this.service = service;
  }

  async getSettings(...conditions: {[key: string]: any}[]) {
    return await this.service.getAll(...conditions);
  }

  async getSetting(...conditions: {[key: string]: any}[]) {
    return await this.service.get(...conditions);
  }

  async createSetting(setting: ISettings) {
    return await this.service.create(setting);
  }

  async updateSetting(conditions: { [key: string]: any }[], setters: { [key: string]: any }[]) {
    return await this.service.update(conditions, setters);
  }

}
