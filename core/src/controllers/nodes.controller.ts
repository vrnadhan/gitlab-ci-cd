import { INode } from '../models/nodes';
import { INodesService } from '../services/nodes.service';

export default class {
  service: INodesService;
  constructor(service: INodesService) {
    this.service = service;
  }

  public getNodes =  async (...conditions: {[key: string]: any}[]) => await this.service.getAll(...conditions);
  public getNode = async (...conditions: {[key: string]: any}[]) => await this.service.get(...conditions);
  public createNode = async (node: INode) => await this.service.create(node);
  public updateNode = async (searchByKey: any, updatedEntity: Partial<INode>) => await this.service.update(searchByKey, updatedEntity);
}
