import { IPagesService } from '../services/pages.service';
import { IPage } from '../models/pages';

export default class {
  service: IPagesService;
  constructor(service: IPagesService) {
    this.service = service;
  }

  async getPages(...conditions: {[key: string]: any}[]) {
    return await this.service.getAll(...conditions);
  }

  async getPage(...conditions: {[key: string]: any}[]) {
    return await this.service.get(...conditions);
  }

  async createPage(page: IPage) {
    return await this.service.create(page);
  }

  async updatePage(conditions: { [key: string]: any }[], setters: { [key: string]: any }[]) {
    return await this.service.update(conditions, setters);
  }
}
