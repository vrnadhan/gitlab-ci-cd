import wrap from '../helpers/wrap'
import express from 'express'
import { NodesController as Controller } from '../controllers'
import { NodesService } from '../services'

const router = express.Router()
const controller = new Controller(new NodesService())

router.get(
  '/',
  wrap(async (req, res) => {
    const { query } = req
    let nodes
    if (query.subscribed) {
      nodes = await controller.getNodes({"subscribe" : true});
    } else {
      nodes = await controller.getNodes();
    }
    res.json(nodes)
  })
)

router.get(
  '/:node',
  wrap(async (req, res) => {
    const { node } = req.params
    // TODO Will this succeed? node is not a property.
    const result = await controller.getNode({node});
    res.json(result);
  })
)

router.get(
  '/subscribe/:node',
  wrap(async (req, res) => {
    const { node } = req.params;

    const result = await controller.updateNode(node, {...node, subscribe: true});
    res.json(result)
  })
)

router.get(
  '/un_subscribe/:node',
  wrap(async (req, res) => {
    const { node } = req.params
    const result = await controller.updateNode(node, {...node, subscribe: false});
    res.json(result)
  })
)

export default router
