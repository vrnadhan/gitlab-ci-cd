import wrap from '../helpers/wrap';
import express from 'express';
import { PagesController as Controller } from '../controllers';
import { PagesService } from '../services';

const router = express.Router();
const controller = new Controller(new PagesService());

router.get(
  '/',
  wrap(async (req, res) => {
    const pages = await controller.getPages();
    console.log("pages.routes.ts ~ line 13 ~ wrap ~ pages", pages);

    res.json(pages)
  })
)

router.get(
  '/:id',
  wrap(async (req, res) => {
    const { id } = req.params;
    const page = await controller.getPage({ id });

    res.json(page)
  })
)

router.post(
  '/',
  wrap(async (req, res) => {
    const { name, url, content, icon, menuPosition } = req.body;
    const page = await controller.createPage(
      { name, url, content, icon, menuPosition }
    );

    res.json(page);
  })
)

router.put(
  '/:id',
  wrap(async (req, res) => {
    const { id } = req.params;
    const { name, url, content, icon, menuPosition } = req.body;
    const page = await controller.updatePage(
      [{ id }],
      [{ name, url, content, icon, menuPosition }]
    );

    res.json(page);
  })
)

export default router;