export { default as NodesRoutes } from "./nodes.routes";
export { default as SettingsRoutes } from "./settings.routes";
export { default as PagesRoutes } from "./pages.routes";
export { default as OPCUARoutes } from "./opcua.routes";