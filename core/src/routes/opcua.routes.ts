import wrap from '../helpers/wrap'
import EventBus from '../models/EventBus'
import express from 'express'
import OPCUA from '../models/OPCUA'

const router = express.Router()

const ip = 'opc.tcp://PTPC:4840'
const opcua = new OPCUA(ip)
async function startOPCUA() {
  try {
    await opcua.connect()
  } catch (e) {
    console.error('Could not connect to OPCUA')
    console.error(e)
  }

  opcua.monitorAllConfiguredNodesFromDatabase()
}

startOPCUA()

process.on('SIGINT', async () => {
  console.log('Caught interrupt signal')
  setTimeout(() => {
    process.exit()
  }, 3000)
  await opcua.close()
})

router.get('/restart', (req, res) => {
  EventBus.emit(TramEvent.RESTART_OPCUA.toString())
  res.json({ msg: 'Restart called' })
})

router.get('/read', async (req, res) => {
  const read = await opcua.read()
  res.json(read)
})

router.get('/read/:variable', async (req, res) => {
  const { variable } = req.params
  const read = await opcua.read(variable)
  res.json(read)
})

router.get('/browse/:variable', async (req, res) => {
  const { variable } = req.params
  const browse = await opcua.session.browse(
    `${opcua.prefix}${opcua.basePath}.${variable}`
  )
  res.json(browse)
})

router.get('/monitor/:variable', async (req, res) => {
  const { variable } = req.params
  EventBus.emit(TramEvent.OPCUA_MONITOR_VARIABLE.toString(), variable)
  res.json({ msg: 'Monitor called' })
})

router.get(
  '/monitored_items',
  wrap(async (req, res) => {
    try {
      const items: any[] = Object.values(opcua.subscription.monitoredItems)
      const list = []
      for (const i of items) {
        list.push(i.itemToMonitor.nodeId)
      }
      res.json({ list })
      return
    } catch (e) {
      console.error(e)
      res.json({ error: e })
    }
  })
)

export default router;