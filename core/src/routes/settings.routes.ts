import wrap from '../helpers/wrap';
import express from 'express';
import { SettingsController as Controller } from '../controllers';
import { SettingsService } from '../services';

const router = express.Router()
const controller = new Controller(new SettingsService())

router.get(
  '/',
  wrap(async (req, res) => {
    const settings = await controller.getSettings()

    res.json(settings)
  })
)

router.get(
  '/:key',
  wrap(async (req, res) => {
    const { key } = req.params
    const setting = await controller.getSetting({ key })

    res.json(setting)
  })
)

router.post(
  '/',
  wrap(async (req, res) => {
    const { key, value, type, defaultValue } = req.body
    const setting = await controller.createSetting(
      {
        key,
        value,
        type,
        defaultValue
      }
    )

    res.json(setting)
  })
)

router.put(
  '/:key',
  wrap(async (req, res) => {
    const { key } = req.params
    const value = req.body
    const setting = await controller.updateSetting([{ key }], [{ value }])

    res.json(setting)
  })
)

export default router;
