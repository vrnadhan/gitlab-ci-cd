// Core support
import express from 'express';
import cors from 'cors';
// Routes
import { SettingsRoutes, NodesRoutes, OPCUARoutes, PagesRoutes } from './routes';

// Misc
import WebsockerServer from './models/WebsocketServer';
import Store from './store';

new Store()
new WebsockerServer()

const app = express()
const port = process.env.PORT || 5000;
const hostname = "0.0.0.0";

app.use(express.json())

app.use(
    cors({
        exposedHeaders: ['Content-Disposition'],
    })
)

app.use('/hmi/api/opcua', OPCUARoutes)
app.use('/hmi/api/nodes', NodesRoutes)
app.use('/hmi/api/pages', PagesRoutes)
app.use('/hmi/api/settings', SettingsRoutes)
app.get('/hmi', (req, res) => res.send("Hello World!"))

app.use((req, res, next) => {
    const err = {
        statusCode: 404,
        message: '404 - Not Found',
    }
    console.log(err)
    next(err)
})

app.use((err, req, res) => {
    console.error(err)
    const statusCode = err.statusCode || 500
    const message = err.stack

    res.status(statusCode).json({ message })
})

app.listen(port, () =>
    console.log(`App listening at http://${hostname}:${port}`)
)