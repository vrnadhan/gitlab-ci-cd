export interface ISettings {
    key: String;
    value: JSON;
    type: String;
    defaultValue: JSON;
}