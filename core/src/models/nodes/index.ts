import { BrowseDescriptionLike } from "node-opcua-client";

export interface INode {
    path: BrowseDescriptionLike;
    subscribe: boolean;
}