import { Server } from 'ws'
import EventBus from './EventBus'

export default class WebsocketServer {
  wss: Server
  HMIclients: any[]
  DAServer: any[]

  constructor() {
    this.wss = new Server({
      port: 5001,
      perMessageDeflate: {
        zlibDeflateOptions: {
          // See zlib defaults.
          chunkSize: 1024,
          memLevel: 7,
          level: 3,
        },
        zlibInflateOptions: {
          chunkSize: 10 * 1024,
        },
        // Other options settable:
        clientNoContextTakeover: true, // Defaults to negotiated value.
        serverNoContextTakeover: true, // Defaults to negotiated value.
        serverMaxWindowBits: 10, // Defaults to negotiated value.
        // Below options specified as default values.
        concurrencyLimit: 10, // Limits zlib concurrency for perf.
        threshold: 1024, // Size (in bytes) below which messages
        // should not be compressed.
      },
    })

    this.HMIclients = []
    this.DAServer = []
    this.initEvents()

    EventBus.on(TramEvent.SEND_TO_CLIENTS.toString(), (data) => {
      this.send(data)
    })

    EventBus.on(TramEvent.SEND_TO_CLIENT.toString(), ({ ws, msg }) => {
      this.sendToClient(ws, msg)
    })
  }

  initEvents(): void {
    this.wss.on('connection', (ws, req) => {
      EventBus.emit(TramEvent.NEW_WEBSOCKET_CLIENT.toString(), ws)

      if (req.url.includes('client=da')) {
        this.DAServer.push(ws)
      }

      ws.on('message', (message: string) => {
        EventBus.emit(
          TramEvent.MESSAGE_FROM_WEBSOCKET_CLIENT.toString(),
          JSON.parse(message)
        )
      })

      ws.on('error', (err) => {
        console.error(err)
      })

      ws.on('close', (code, reason) => {})
    })
  }

  sendToClient(ws: WebSocket, msg: JSON) {
    ws.send(JSON.stringify(msg))
  }

  send(data: JSON): void {
    for (const client of this.wss.clients) {
      client.send(JSON.stringify(data))
    }
  }
}
