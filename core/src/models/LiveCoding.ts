import {
  OPCUAClient,
  ClientSession,
  makeBrowsePath,
  BrowsePathResult,
  NodeId,
  BrowseDirection,
} from 'node-opcua'

const endpointUrl = 'opc.tcp://opcuademo.sterfive.com:26545'
const endpointUrl1 = 'opc.tcp://172.25.144.5:4840'
async function doStuff(session: ClientSession) {

    const array = await session.readNamespaceArray();
console.log(array);

  const ns = 3
  const nsDI = 2;
  const nsCodesys = 4;
  // /${nsDI}:DeviceSet/${nsCodesys}:CODESYS Control RTE x64 /
  const browsePath = makeBrowsePath(
      'i=85', /* ObjectFolder*/
      //  "ns=4;s=|var|CODESYS Control RTE x64 .Application.Global_OPC_Variabels", ns=4;s=|plc|CODESYS Control RTE x64
      `/${nsDI}:DeviceSet/${nsCodesys}:CODESYS Control RTE x64 /${ns}:Resources/${nsCodesys}:Application/${ns}:GlobalVars/${nsCodesys}:OPC_Par`)

  const browsePathResult = await session.translateBrowsePath(browsePath)

  console.log(browsePathResult.toString())

  const globalVariablesNodeId: NodeId =  browsePathResult.targets[0].targetId;
console.log("globalVariablesNodeId = ", globalVariablesNodeId.toString());


   let  browseResult = await session.browse({
        nodeId: globalVariablesNodeId,
        browseDirection: BrowseDirection.Forward,
        includeSubtypes: true,
        referenceTypeId: "HasChild",
        nodeClassMask: 0x0,
        resultMask: 0xFF

   });

   
   console.log(browseResult.toString());
   for (const ref of browseResult.references!) {
    //   console.log(ref.nodeId.toString(), ref.browseName.toString());

   }
   while(browseResult.continuationPoint) {
      browseResult = await session.browseNext(browseResult.continuationPoint, true);
      for (const ref of browseResult.references!) {
        //   console.log(ref.nodeId.toString(), ref.browseName.toString());
       }
      console.log(" Here are some more ! ", browseResult.references!.length);
   }

}
;(async () => {
  try {
    const client = OPCUAClient.create({
      endpointMustExist: false,
    })

    await client.connect(endpointUrl1)

    const session = await client.createSession()

    await doStuff(session)

    await session.close()

    await client.disconnect()
  } catch (err) {
    console.log(err)
    process.exit(1)
  }
})()
