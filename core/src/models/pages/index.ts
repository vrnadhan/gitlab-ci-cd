export interface IPage {
    id?: String;
    name: String;
    url: String;
    content: JSON;
    icon: String;
    menuPosition: number;
}