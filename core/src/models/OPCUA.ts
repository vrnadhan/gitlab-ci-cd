import {
  OPCUAClient,
  MessageSecurityMode,
  SecurityPolicy,
  AttributeIds,
  ClientSubscription,
  ClientMonitoredItemGroup,
  TimestampsToReturn,
  MonitoringParametersOptions,
  ReadValueIdOptions,
  ClientMonitoredItem,
  DataValue,
  DataType,
  BrowseDescriptionLike,
  NodeIdLike,
  ClientSession,
  StatusCodes,
  makeNodeId,
  VariableIds,
} from 'node-opcua'
import { createWriteStream, WriteStream, createReadStream } from 'fs';
import Path from 'path';
import * as readline from 'readline'
import Postgres from '../database/postgres'
import EventBus from './EventBus'
import OPCValue from '@hmi/types/OPCValue'

const PRIVATE_KEY_FILE = Path.resolve(
  __dirname,
  '..',
  '..',
  '..',
  'certificates/PKI/own/private/',
  'private_key.pem',
);
export default class OPCUA {
  subscription: ClientSubscription
  session: ClientSession
  client: OPCUAClient
  nodes: [BrowseDescriptionLike]
  prefix: string
  basePath: string
  fileWriter: WriteStream
  db: Postgres
  writeCounter: number

  constructor(public ip: string) {
    this.ip = ip
    this.prefix = 'ns=4;s='
    this.basePath =
      '|var|CODESYS Control RTE x64 .Application.Global_OPC_Variabels'
    this.db = new Postgres()

    this.writeCounter = 0

    EventBus.on(
      TramEvent.OPC_WRITE_VALUE.toString(),
      ({ key, value, dataType }) => {
        this.write(key, value, dataType)
      }
    )

    EventBus.on(TramEvent.RESTART_OPCUA.toString(), async () => {
      await this.close()
      await this.sleep(1000)
      await this.connect()
      this.monitorAllConfiguredNodesFromDatabase()
    })

    EventBus.on(TramEvent.OPCUA_MONITOR_VARIABLE.toString(), (variable) => {
      this.monitor(variable)
    })

    console.log(DataType[1].toString())
  }

  async write(key: string, value: any, dataType: number) {
    this.writeCounter = this.writeCounter + 1
    const i = this.writeCounter
    let nodeToWrite = {
      nodeId: this.prefix + this.basePath + '.' + key,
      attributeId: AttributeIds.Value,
      value: {
        sourceTimestamp: new Date(),
        statusCode: StatusCodes.Good,
        value: {
          dataType,
          value: value,
        },
      },
    }
    if (this.session) {
      if (!this.session.isReconnecting) {
        try {
          await this.session.write(nodeToWrite)
        } catch (e) {
          console.log(key, value, dataType)
          console.log(e)
        }
      } else {
        //console.error('Session is reconnecting')
      }
    }
  }

  async connect() {
    const connectionStrategy = {
      initialDelay: 100,
      maxRetry: 10,
    }
    const options = {
      applicationName: 'HMI',
      connectionStrategy: connectionStrategy,
      securityMode: MessageSecurityMode.SignAndEncrypt,
      securityPolicy: SecurityPolicy.Basic256Sha256,
      endpoint_must_exist: true,
      tokenRenewalInterval: 1000 * 60 * 1, // 0 -> 75% of defaultSecureTokenLifetime
      //defaultSecureTokenLifetime: 60*1000,
      certificateFile: 'my_certificate.pem',
      privateKeyFile: PRIVATE_KEY_FILE,
      keepSessionAlive: true,
      keepAlive: true,
      requestedSessionTimeout: 1200000,
    }
    this.client = OPCUAClient.create(options)
    this.client.on('backoff', (retry, delay) => {
      console.log('Backoff ', retry, ' next attempt in ', delay, 'ms')
    })

    this.client.on('connection_lost', () => {
      console.log('Connection lost')
      this.close()
    })

    this.client.on('connection_reestablished', () => {
      console.log('Connection re-established')
      //this.monitorAllConfiguredNodesFromDatabase()
    })

    this.client.on('connection_failed', () => {
      console.log('Connection failed')
    })
    this.client.on('start_reconnection', () => {
      console.log('Starting reconnection')
    })

    this.client.on('after_reconnection', (err) => {
      console.log('After Reconnection event =>', err)
    })

    this.client.on('Client timed_out_request', () => {
      console.log('timed_out_request')
    })

    await this.client.connect(this.ip)

    this.session = await this.client.createSession()

    console.log(
      ' the timeout value set by the server is ',
      this.session.timeout,
      ' ms'
    )

    this.session.on('session_restored', () => {
      console.log('Session restored')
    })

    this.session.on('keepalive', () => {
      console.log('Session keepalive')
    })

    this.session.on('keepalive_failure', () => {
      console.log('Session keepalive_failure')
    })

    this.session.on('session_closed', (statusCode) => {
      console.log(statusCode)
      console.log("gosh ! session timeout was too short ! let's increase it")
    })

    this.subscription = ClientSubscription.create(this.session, {
      requestedPublishingInterval: 250,
      requestedLifetimeCount: 200,
      requestedMaxKeepAliveCount: 20,
      maxNotificationsPerPublish: 1000,
      publishingEnabled: true,
      priority: 10,
    })

    this.subscription
      .on('started', (subscriptionId) => {
        console.log(
          'subscription started for 2 seconds - subscriptionId=',
          subscriptionId
        )
        console.log(
          'publishingInterval: ',
          this.subscription.publishingInterval
        )
      })
      .on('error', (e) => {
        console.error('Err in subscirption')
        console.error(e)
      })
      .on('item_added', (item) => { })
      .on('status_changed', (status) => { })
      .on('received_notifications', (n) => { })
      .on('internal_error', (e) => {
        console.error('Subscription internal_error')
        console.error(e)
      })
      .on('keepalive', () => {
        // console.log('Subscription keepalive')
      })
      .on('terminated', () => {
        console.log('terminated')
      })
  }

  openFileWriter(filePath: string): void {
    this.fileWriter = createWriteStream(filePath, {
      flags: 'a',
    })
  }

  endFileWriter(): void {
    this.fileWriter.end()
  }

  async readNodesFromFile(): Promise<any> {
    return new Promise((res, rej) => {
      let counter = 0
      const nodes = []
      const rl = readline.createInterface({
        input: createReadStream('opcua-paths.txt'),
        output: process.stdout,
        terminal: false,
      })

      rl.on('line', (line) => {
        counter++
        //this.monitor(line)
        nodes.push(line)
      })

      rl.on('close', () => {
        console.log('End, ', counter)
        res(nodes)
      })
    })
  }

  async browseAndStoreInFile(): Promise<any> {
    const date = new Date()
    this.openFileWriter(
      `opcua-paths-${date.getFullYear()}-${date.getMonth() + 1
      }-${date.getDate()}.txt`
    )
    const root = makeNodeId(VariableIds.Server_NamespaceArray)
    // ns=4;s=|plc|CODESYS Control RTE x64
    // ns=0;i=84
    await this.browse(
      `${this.prefix}${this.basePath}` as BrowseDescriptionLike,
      'FILE'
    )
    this.endFileWriter()
  }

  async browseAndStoreInDatabase(): Promise<any> {
    await this.browse(this.basePath, 'DATABASE')
    this.endFileWriter()
  }

  async browse(path: BrowseDescriptionLike, action?: string) {
    try {
      const browseResult = await this.session.browse(`${path}`)
      console.log(browseResult.references)
      if (browseResult.references.length) {
        for (const reference of browseResult.references) {
          console.log(reference.nodeId)
          const identifierType = reference.nodeId.identifierType
          const ns = reference.nodeId.namespace
          const nodeIdValue = reference.nodeId.value

          const nextPath = `ns=${ns};${identifierType === 2 ? 's' : 'i'
            }=${nodeIdValue}`

          if (reference.browseName.toString().includes('[')) {
            const browseResultArray = await this.session.browse(nextPath)
            if (!browseResultArray.references.length) {
              this.doBrowseAction(
                `${reference.nodeId.value.toString().replace(/\[.*/, '')}`,
                action
              )
              break
            }
          }
          if (!reference.browseName.toString().includes('place_holder')) {
            await this.browse(nextPath, action)
          } else {
            console.log('Skipped: ', reference.browseName.toString())
          }
        }
      } else {
        console.log(path)
        if (action) {
          this.doBrowseAction(path, action)
        } else {
          return path
        }
      }
    } catch (e) {
      console.error(e)
    }
  }

  doBrowseAction(path: BrowseDescriptionLike, action: string) {
    if (action === 'FILE') {
      this.fileWriter.write(`${path}\n`)
    }
    if (action === 'LOG') {
      console.log(path)
    }
    if (action === 'MONITOR') {
      this.monitor(`${this.prefix}${path}`)
    }
    if (action === 'DATABASE') {
      this.db.query(
        `INSERT INTO nodes (node, subscribe) VALUES(${path}, false)`,
        []
      )
    }
  }

  valueChanged(node: NodeIdLike, dataValue: DataValue) {
    const data: OPCValue = {
      key: node
        .toString()
        .replace(
          /ns=4;s=\|var\|CODESYS Control RTE x64 .Application.Global_OPC_Variabels./,
          ''
        ),
      value: dataValue.value.value,
      dataType: dataValue.value.dataType,
    }

    EventBus.emit(TramEvent.OPC_VALUE_CHANGED.toString(), data)
  }

  async monitor(node: NodeIdLike) {
    const itemToMonitor: ReadValueIdOptions = {
      nodeId: `${this.prefix}${this.basePath}.${node}`,
      attributeId: AttributeIds.Value,
    }
    const parameters: MonitoringParametersOptions = {
      samplingInterval: 100,
      discardOldest: true,
      queueSize: 10,
    }
    const monitoredItem = ClientMonitoredItem.create(
      this.subscription,
      itemToMonitor,
      parameters,
      TimestampsToReturn.Both
    )

    monitoredItem.on('err', (e) => {
      console.error('Monitored item err')
      console.error(e)
    })

    monitoredItem.on('terminated', () => {
      console.log('Terminated item')
    })

    monitoredItem.on('changed', (dataValue: DataValue) => {
      this.valueChanged(node, dataValue)
    })
  }

  async monitorAllConfiguredNodesFromFile() {
    const nodes = await this.readNodesFromFile()
    let index = 0
    while (index < nodes.length) {
      await this.monitorGroup(nodes.slice(index, index + 1500))
      index += 1500
    }
  }

  async read(node?: String) {
    if (node) {
      return this.session.read({
        nodeId: `${this.prefix}${this.basePath}.${node}`,
        attributeId: AttributeIds.Value,
      })
    }
    return this.session.read({
      nodeId: `${this.prefix}${this.basePath}`,
      attributeId: AttributeIds.Value,
    })
  }

  async monitorAllConfiguredNodesFromDatabase() {
    const nodes = await this.db.query(
      'SELECT * FROM nodes WHERE subscribe = true',
      []
    )

    const inc = 20
    let index = 0
    while (index < nodes.length) {
      await this.monitorGroup(
        nodes.slice(index, index + inc).map(({ node }) => {
          return `${this.prefix}${this.basePath}.${node}`
        })
      )
      await this.sleep(5000)
      index += inc
    }
  }

  async sleep(ms: number): Promise<void> {
    return new Promise<void>((res) => {
      setTimeout(() => {
        res()
      }, ms)
    })
  }

  async monitorGroup(nodes: NodeIdLike[]) {
    const optionsGroup = {
      discardOldest: true,
      queueSize: 1,
      samplingInterval: 100,
    }

    const itemsToMonitor = nodes.map((x) => {
      return { nodeId: x, attributeId: AttributeIds.Value }
    })

    const monitoredItemGroup = ClientMonitoredItemGroup.create(
      this.subscription,
      itemsToMonitor,
      optionsGroup,
      TimestampsToReturn.Both
    )

    monitoredItemGroup.on('initialized', () => {
      console.log('Initialized !')
      console.log(nodes)
    })

    monitoredItemGroup.on('changed', (monitoredItem, dataValue) => {
      this.valueChanged(monitoredItem.itemToMonitor.nodeId, dataValue)
    })

    monitoredItemGroup.on('terminated', () => {
      console.log('terminated ')
    })

    monitoredItemGroup.on('err', (err) => {
      console.error(err)
    })
  }

  async close() {
    for (const item of Object.values(this.subscription.monitoredItems)) {
      await item.terminate()
    }
    await this.subscription.terminate()
    await this.session.close()
    this.session = null
    await this.client.disconnect()
  }
}
