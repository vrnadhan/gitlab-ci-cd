import { EventEmitter } from 'events'

class EventBus extends EventEmitter {
  static instance: EventBus

  constructor() {
    super()
    if (!EventBus.instance) {
      EventBus.instance = this
    }
    return EventBus.instance
  }

  removeAllListenersForAllEvents() {
    const events = this.eventNames()
    for (const event of events) {
      this.removeAllListeners(event)
    }
  }
}
const eventbus = new EventBus()
export default eventbus
