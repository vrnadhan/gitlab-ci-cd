import EventBus from './EventBus'

export default class AchievedLevel {
  list: number[]
  pourTime: number
  topOfMold: number
  levelScale: number
  nominalValue: number

  constructor() {
    this.list = new Array<number>()
  }

  setPourTime(time: number) {
    this.pourTime = time
  }

  setTopOfMold(topOfMold: number) {
    this.topOfMold = topOfMold
  }

  setLevelScale(levelScale: number) {
    this.levelScale = levelScale
  }

  setNominalValue(nominalValue: number) {
    this.nominalValue = nominalValue
  }

  // Level_in_mm := (Level_in_bits - Par.SENS_top_off_mold) ) / Par.SENS_level_scale + Par.SENS_nominal_value;

  bitTomm(bit: number): number {
    return Math.round(
      (bit - this.topOfMold) / this.levelScale + this.nominalValue
    )
  }

  addValue(arr: number[]) {
    const diff = []

    if (!this.list.length) {
      this.list = Object.values(arr)
        .slice(0, this.pourTime * 25 + 25 * 2)
        .map((x) => this.bitTomm(x))
      EventBus.emit(TramEvent.SEND_TO_CLIENTS.toString(), {
        type: 'AchievedLevel',
        list: this.list,
      })
    } else {
      for (let i = 0; i < this.pourTime * 25 + 25 * 2; i++) {
        if (this.list[i] !== this.bitTomm(arr[i])) {
          diff.push({ i, v: arr[i] })
        }
      }
      this.list = Object.values(arr)
        .slice(0, this.pourTime * 25 + 25 * 2)
        .map((x) => this.bitTomm(x))
      EventBus.emit(TramEvent.SEND_TO_CLIENTS.toString(), {
        type: 'AchievedLevel',
        diff,
        list: this.list,
      })
    }
  }
}
