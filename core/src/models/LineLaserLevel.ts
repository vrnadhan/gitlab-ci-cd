export default class LineLaserLevel {
  list: number[]
  length: number

  constructor() {
    this.list = new Array<number>()
    this.length = 310
  }

  addValue(value: number) {
    if (this.list.length > 310) {
      this.list.shift()
    }
    this.list.push(value)
  }
}
