export interface Repo<T> {
    // Get all entities that matches the given conditions
    getAll(...conditions: {[key: string]: any}[]): Promise<T[]>;
    // Get a single entity that matches the given conditions
    get(...conditions: {[key: string]: any}[]): Promise<T>;
    // Create an entity
    create(entity: Partial<T>): Promise<T>;
    // Update all entities
    update(conditions: { [key: string]: any }[], setters: { [key: string]: any }[]): Promise<T>;
}