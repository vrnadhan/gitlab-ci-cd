import { OPCControl } from './OPCControl'

export interface BottomBarIndicator extends OPCControl {
  order: number
  value: number
  opckey: string
  name: string
}
