import OPCValue from './OPCValue'

export default interface OPCValueList {
  [key:string]: OPCValue
}