export default interface OPCValue {
  readonly key: string
  value: any
  dataType: number
}
