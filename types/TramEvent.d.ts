declare const enum TramEvent {
  // Called when new Websocket client connectes
  NEW_WEBSOCKET_CLIENT,
  // Remove
  OPC_UPDATE,
  // Called when OPC varible chnaged from OPC server
  OPC_VALUE_CHANGED,
  // Write value to OPC server
  OPC_WRITE_VALUE,
  // Closes opcua connection and restarts the connection
  RESTART_OPCUA,
  // Sends data to websocket clients
  SEND_TO_CLIENTS,
  // Send to a specific client
  SEND_TO_CLIENT,
  // Message from Websocket client
  MESSAGE_FROM_WEBSOCKET_CLIENT,
  // Monitor opcua variable
  OPCUA_MONITOR_VARIABLE,
}
