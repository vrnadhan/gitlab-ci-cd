export interface OPCControl {
  readonly key: string
  name: string
  unit?: string
  input: boolean
}
