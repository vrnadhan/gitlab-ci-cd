/* eslint-env browser */
import axios from 'axios'
const API_URL =
  process &&
  process.env &&
  process.env.NODE_ENV &&
  process.env.NODE_ENV == 'development'
    ? '172.25.144.5'
    : window.location.href.split('//')[1].split('/')[0]
const BASE = `${window.location.protocol}//${API_URL}/hmi/api`
// const BASE = `http://172.25.144.5/hmi/api/`

const apiClient = axios.create({
  baseURL: BASE,
  responseType: 'json',
  headers: {
    'Content-Type': 'application/json',
  },
})

export async function getNodes() {
  const { data } = await apiClient.get('/nodes')
  return data
}

export async function getNode(node: string) {
  const { data } = await apiClient.get(`/nodes/${node}`)
  if (data[0]) {
    return data[0]
  }
  return data
}

export async function subscribeNode(node: string) {
  const { data } = await apiClient.get(`/nodes/subscribe/${node}`)
  return data
}

export async function unSubscribeNode(node: string) {
  const { data } = await apiClient.get(`/nodes/un_subscribe/${node}`)
  return data
}

export async function getPages() {
  const { data } = await apiClient.get('/pages')
  return data
}