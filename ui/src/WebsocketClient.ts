import state from './store'
import lineLaserState from './state/LineLaser'

export default class WebsocketClient {
  ws: WebSocket

  constructor() {
    const API_URL =
      process &&
        process.env &&
        process.env.NODE_ENV &&
        process.env.NODE_ENV == 'development'
        ? '172.25.144.5'
        : window.location.href.split('//')[1].split('/')[0]
    this.ws = new WebSocket(`ws://${API_URL}/hmi-ws`)
    // this.ws = new WebSocket(`ws://172.25.144.5/hmi-ws`)
    this.connect()
  }

  connect() {
    this.ws.onmessage = (event: MessageEvent) => {
      const obj = JSON.parse(event.data)
      if (obj.type === 'LineLaserGraph') {
        lineLaserState.setData(obj.list)
      }

      state.dispatch('setOPCData', obj)
    }

    this.ws.onclose = () => {
      setTimeout(() => {
        this.connect()
      }, 300)
    }

    this.ws.onerror = () => {
      this.ws.close()
    }
  }

  send(data: JSON) {
    this.ws.send(JSON.stringify(data))
  }
}
