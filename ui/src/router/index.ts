import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'
import { Component } from 'vue'
import Layout from '../views/Layout.vue'
import Admin from '../views/Admin.vue'
import GlobalOPC from '../views/GlobalOPC.vue'
import Datapoints from '../views/Datapoints.vue'
import TestComponents from '../views/TestComponents.vue'

const routes: RouteRecordRaw[] = [
  {
    path: '/admin',
    name: 'Admin',
    component: (Admin as unknown) as Component,
    children: [
      {
        name: 'globalopc',
        path: 'globalopc',
        component: (GlobalOPC as unknown) as Component,
      },
      {
        name: 'datapoints',
        path: 'datapoints',
        component: (Datapoints as unknown) as Component,
      },
    ],
  },
  {
    path: '/',
    name: 'Layout',
    component: (Layout as unknown) as Component,
  },
  {
    path: '/testcomponents',
    name: 'TestComponents',
    component: (TestComponents as unknown) as Component,
  },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes,
})

export default router
