export default [
  {
    component: 'AlarmsList',
    class: 'row-start-1 col-start-1 row-span-6 col-span-6',
    name: 'Current Alarms',
    columns: [
      {
        title: 'Alarm #',
        objectKey: 'nr',
      },
      {
        title: 'Time On',
        objectKey: 'Time_on',
      },
      {
        title: 'Time Off',
        objectKey: 'Time_off',
      },
      {
        title: 'Alarm',
        objectKey: 'name',
      },
    ],
  },
  {
    component: 'AlarmsList',
    class: 'row-start-7 col-start-1 row-span-6 col-span-6',
    name: 'Old Alarms',
    columns: [
      {
        title: 'Alarm #',
        objectKey: 'nr',
      },
      {
        title: 'Alarm',
        objectKey: 'name',
      },
    ],
  },
  {
    component: 'alarmInfo',
    class: 'row-start-4 col-start-7 row-span-6 col-span-6',
  },
]
