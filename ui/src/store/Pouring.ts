export default [
  {
    component: 'BasePanel',
    class: 'row-start-2 col-start-1',
    columnSize: 24,
    rowSize: 32,
    content: [
      {
        component: 'pouringCanvas',
        class: 'row-span-12 col-span-24',
      },
      // {
      //   component: 'RodPositionChart',
      //   class: 'row-start-5 col-start-1 row-span-3 col-span-6',
      // },
    ],
  },
  {
    component: 'BasePanel',
    class: 'row-start-39 col-start-1',
    columnSize: 24,
    rowSize: 5,
    content: [
      {
        component: 'BaseEditOpcNumber',
        opckey: 'Par.POUR_Freq_rot',
        nameRef: 'RodRotationHz',
        labelPosition: 'label-left',
        columnSize: 8,
        rowSize: 5,
      },
      {
        component: 'BaseEditOpcNumber',
        opckey: 'Par.POUR_Freq_clpl',
        nameRef: 'CleaningPlungeHz',
        labelPosition: 'label-left',
        columnSize: 8,
        rowSize: 5,
      },
      {
        component: 'BaseEditOpcNumber',
        opckey: 'Par.ACT_Force_close',
        nameRef: 'ClosingForcePercent',
        labelPosition: 'label-left',
        columnSize: 8,
        rowSize: 5,
      },
    ],
  },
  {
    component: 'Tabs',
    class: 'row-start-1 col-start-25',
    columnSize: 24,
    rowSize: 32,
    tabs: [
      {
        text: 'Pour',
      },
      {
        text: 'Easypour',
      },
      {
        text: 'Pour 2',
      },
      {
        text: 'Inoc',
      },
      {
        text: 'Positioning',
      },
      {
        text: 'Rodcurve',
      },
    ],
    content: [
      // Pour
      {
        component: 'BasePanel',
        class: 'row-start-4',
        columnSize: 24,
        rowSize: 28,
        tab: 'Pour',
        content: [
          {
            component: 'BaseEditOpcNumberButtons',
            opckey: 'Pattern_Edit.pour_time',
            nameRef: 'PourTime',
            columnSize: 8,
            rowSize: 7,
          },
          {
            component: 'BaseEditOpcNumberButtons',
            opckey: 'Pattern_Edit.rod_offset',
            nameRef: 'RodOffset',
            columnSize: 8,
            rowSize: 7,
          },
          {
            component: 'BaseEditOpcNumberButtons',
            opckey: 'Pattern_Edit.sensitivity',
            nameRef: 'Sensitivity',
            columnSize: 8,
            rowSize: 7,
          },
          {
            component: 'BaseEditOpcNumber',
            opckey: 'Pattern_Edit.level_1',
            nameRef: 'L1',
            columnSize: 8,
          },
          {
            component: 'BaseEditOpcNumber',
            opckey: 'Pattern_Edit.level_2',
            nameRef: 'L2',
            columnSize: 8,
          },
          {
            component: 'BaseEditOpcNumber',
            opckey: 'Pattern_Edit.number_of_levels',
            nameRef: 'NbrOfLevels',
            columnSize: 8,
          },
          {
            component: 'BaseEditOpcNumber',
            opckey: 'Pattern_Edit.number_of_levels',
            nameRef: 'NbrOfLevels',
            columnSize: 8,
          },
          {
            component: 'BaseEditOpcNumber',
            opckey: 'Pattern_Edit.time_ramp',
            nameRef: 'TR1',
            columnSize: 8,
          },
          {
            component: 'BaseEditOpcNumber',
            opckey: 'Pattern_Edit.time_ramp',
            nameRef: 'TR2',
            columnSize: 8,
            conditions: {
              opckey: 'Pattern_Edit.number_of_levels',
              values: [2, 3, 4],
            },
          },
          {
            component: 'BaseEditOpcNumber',
            opckey: 'Pattern_Edit.time_ramp',
            nameRef: 'TR3',
            columnSize: 8,
            conditions: {
              opckey: 'Pattern_Edit.number_of_levels',
              values: [3, 4],
            },
          },
          {
            component: 'BaseEditOpcNumber',
            opckey: 'Pattern_Edit.time_L1',
            nameRef: 'TL1',
            columnSize: 8,
            conditions: {
              opckey: 'Pattern_Edit.number_of_levels',
              values: [2, 3, 4],
            },
          },
          {
            component: 'BaseEditOpcNumber',
            opckey: 'Pattern_Edit.time_L1_L2',
            nameRef: 'TL1L2',
            columnSize: 8,
            conditions: {
              opckey: 'Pattern_Edit.number_of_levels',
              values: [2, 3, 4],
            },
          },
          {
            component: 'BaseEditOpcNumber',
            opckey: 'Pattern_Edit.time_L2',
            nameRef: 'TL2',
            columnSize: 8,
            conditions: {
              opckey: 'Pattern_Edit.number_of_levels',
              values: [3, 4],
            },
          },
          {
            component: 'BaseEditOpcNumber',
            opckey: 'Pattern_Edit.time_L2_L3',
            nameRef: 'TL2L3',
            columnSize: 8,
            conditions: {
              opckey: 'Pattern_Edit.number_of_levels',
              values: [3, 4],
            },
          },
          {
            component: 'BaseEditOpcNumber',
            opckey: 'Pattern_Edit.time_boost',
            nameRef: 'TimeBoost',
            columnSize: 8,
          },
          {
            component: 'BaseEditOpcNumber',
            opckey: 'Pattern_Edit.ext_pour_offset',
            nameRef: 'ExternalOffset',
            columnSize: 8,
          },
          {
            component: 'BaseEditOpcNumber',
            opckey: 'Pattern_Edit.boost_offset',
            nameRef: 'BoostOffset',
            columnSize: 8,
          },
          {
            component: 'BaseEditOpcNumber',
            opckey: 'Pattern_Edit.min_fin_lev',
            nameRef: 'MinFinalLevel',
            columnSize: 8,
          },
        ],
      },
      // Easypour
      {
        component: 'BasePanel',
        class: 'row-start-4',
        columnSize: 24,
        rowSize: 28,
        tab: 'Easypour',
        content: [
          {
            component: 'BaseEnumOpc',
            enums: 'easyPourStates',
            class: 'col-start-8',
            nameRef: 'Status',
            columnSize: 10,
            rowSize: 6,
          },
          {
            component: 'BaseButtonOpc',
            class: 'row-start-20 col-start-5',
            nameRef: 'Start',
            opckey: 'EASYPOUR_Start',
          },
          {
            component: 'BaseButtonOpc',
            class: 'row-start-20 col-start-9',
            nameRef: 'Stop',
            opckey: 'EASYPOUR_Stop',
          },
          {
            component: 'BaseButtonOpc',
            class: 'row-start-20 col-start-13',
            opckey: '123',
            nameRef: 'Good',
          },
          {
            component: 'BaseButtonOpc',
            class: 'row-start-20 col-start-17',
            opckey: '123',
            nameRef: 'Bad',
          },
        ],
      },
      // end
      // Pour 2
      {
        component: 'BasePanel',
        class: 'row-start-4',
        columnSize: 24,
        rowSize: 28,
        tab: 'Pour 2',
        content: [
          {
            component: 'BaseEditOpcNumber',
            opckey: 'Pattern_Edit.pre_start_pour',
            nameRef: 'PreStartPourSeconds',
          },
          {
            component: 'BaseEditOpcNumber',
            opckey: 'Pattern_Edit.terminate_rod_position',
            nameRef: 'TerminateRodPosition',
          },
          {
            component: 'BaseEditOpcNumber',
            opckey: 'Pattern_Edit.Rod_offset_ratio',
            nameRef: 'RodOffsetRatio',
          },
          {
            component: 'BaseSwitchButtonOpc',
            opckey: 'Pattern_Edit.Extend_Pouring_Enable',
            nameRef: 'ExtendPouring',
          },
          {
            component: 'BaseSwitchButtonOpc',
            opckey: 'Pattern_Edit.terminate_pour_enable',
            nameRef: 'TerminatePour',
          },
          {
            component: 'BaseSwitchButtonOpc',
            opckey: 'Pattern_Edit.Fill_up_Enabled',
            nameRef: 'FillUpEnabled',
          },
        ],
      },
      // end
      // Inoc
      {
        component: 'BasePanel',
        class: 'row-start-4',
        columnSize: 24,
        rowSize: 28,
        tab: 'Inoc',
        content: [
          {
            component: 'BaseEditOpcNumber',
            opckey: 'Pattern_Edit.inoc_rate',
            nameRef: 'InoculationRateGramsPerSecond',
          },
          {
            component: 'BaseEditOpcNumber',
            opckey: 'Pattern_Edit.use_filter',
            nameRef: 'InocRateWireMMPerSecond',
          },
          {
            component: 'BaseEditOpcNumber',
            opckey: 'Pattern_Edit.nom_mold_weight',
            nameRef: 'MoldWeightKilogram',
          },
          {
            component: 'modalSelector',
            class: 'row-start-12 col-start-7',
            opckey: 'INOC_Adjustment_Mode',
            nameRef: 'InoculationModes',
            columnSize: 12,
            selections: [
              'Manual inoculation rate',
              'Use customer database',
              'Adjust Customer database rate',
              'Use analyzed rate',
            ],
          },
        ],
      },
      // end
      // Positioning
      {
        component: 'BasePanel',
        class: 'row-start-4',
        columnSize: 24,
        rowSize: 28,
        tab: 'Positioning',
        content: [
          {
            component: 'BaseEditOpcNumber',
            class: 'row-start-9',
            opckey: 'Pattern_Edit.lt_position_offset',
            nameRef: 'LTPositionOffset',
          },
          {
            component: 'BaseEditOpcNumber',
            class: 'row-start-9',
            opckey: 'Pattern_Edit.ByPass_Nth_mold',
            nameRef: 'BypassNthMold',
          },
          {
            component: 'BaseEditOpcNumber',
            class: 'row-start-9',
            opckey: 'Pattern_Edit.PP_Thickness',
            nameRef: 'PPThickness',
          },
        ],
      },
      // end
    ],
  },
  {
    component: 'patternControl',
    class: 'row-start-39 col-start-25',
    columnSize: 25,
  },
]
