export default [
  {
    component: 'Tabs',
    class: '',
    columnSize: 48,
    rowSize: 48,
    tabs: [
      {
        text: 'Parameters',
      },
      {
        text: 'Parameters 2',
      },
    ],
    content: [
      // Parameters
      {
        component: 'BasePanel',
        class: 'row-start-1 col-start-42',
        columnSize: 6,
        rowSize: 48,
        tab: 'Parameters',
        content: [
          {
            opckey: 'Par.HMI_Password_protected_patterns',
            component: 'BaseEnumOpc',
            nameRef: 185,
            enums: ['no', 'yes'],
            columnSize: 6,
          },
          {
            opckey: 'Par.HMI_autosave_patterns',
            component: 'BaseEnumOpc',
            nameRef: 191,
            enums: ['no', 'yes'],
            columnSize: 6,
          },
          {
            opckey: 'Par.HMI_Language',
            component: 'BaseEnumOpc',
            nameRef: 186,
            enums: 'languages',
            columnSize: 6,
          },
          {
            name: 'HMI Version',
            opckey: 'SYS_HMI_Ver',
            component: 'BaseOpcNumber',
            columnSize: 6,
          },
          {
            name: 'Days until License will expire',
            opckey: 'HMI_License_time',
            component: 'BaseOpcNumber',
            columnSize: 6,
          },
          {
            name: 'License Key',
            opckey: 'Par.HMI_License_code',
            component: 'BaseEditOpcString',
            columnSize: 6,
          },
          {
            name: 'Shutdown PTPC',
            columnSize: 6,
            class: 'row-start-10',
            component: 'BaseButtonFunction',
            btnFunction: () => {
              console.log('Shutting down PTPC from HMI not implemented')
            },
          },
          {
            name: 'Reload HMI',
            columnSize: 6,
            class: 'row-start-33',
            component: 'BaseButtonFunction',
            btnFunction: () => {
              location.reload()
            },
          },
        ],
      },
      {
        component: 'BasePanel',
        class: 'row-start-1 col-start-1',
        columnSize: 9,
        rowSize: 15,
        tab: 'Parameters',
        content: [
          {
            component: 'BaseSwitchButtonOpc',
            name: 'Pour Data',
            opckey: 'DIAG_Store_Pour_data',
            class: 'row-start-1',
            textRight: true,
          },
          {
            component: 'BaseSwitchButtonOpc',
            name: 'Ram Data',
            opckey: 'DIAG_Store_Ram_Pos_data',
            class: 'row-start-6',
            textRight: true,
          },
          {
            component: 'BaseSwitchButtonOpc',
            name: 'Drivers Data',
            opckey: 'DIAG_Store_Drivers_data',
            class: 'row-start-9',
            textRight: true,
          },
          {
            component: 'BaseSwitchButtonOpc',
            name: 'Sensor Data',
            opckey: 'DIAG_Store_SENS_data',
            class: 'row-start-12',
            textRight: true,
          },
          {
            component: 'BaseSwitchButtonOpc',
            name: 'CamCom Data',
            opckey: 'DIAG_Store_Cam_Com_data',
            class: 'row-start-15',
            textRight: true,
          },
          {
            component: 'BaseOpcNumber',
            name: 'Buffer Index',
            opckey: 'Store_data_buffer_row',
            columnSize: 3,
          },
          {
            component: 'BaseButtonOpc',
            opckey: 'FILE_Store_diag_buffer_File',
            name: 'Store Buffer File',
            class: 'row-start-15 col-start-9',
          },
        ],
      },
      {
        component: 'BasePanel',
        class: 'row-start-25 col-start-1',
        columnSize: 9,
        rowSize: 15,
        tab: 'Parameters',
        content: [
          {
            component: 'BaseSwitchButtonOpc',
            name: 'Log SYNC',
            opckey: 'DIAG_Store_Sync_Log',
            class: 'row-start-1',
            textRight: true,
          },
          {
            component: 'BaseOpcNumber',
            name: 'Log Index',
            opckey: 'LOG_buff_ind',
            columnSize: 3,
          },
          {
            component: 'BaseButtonOpc',
            opckey: 'FILE_Store_log_buffer_File',
            name: 'Store Log File',
            class: 'row-start-15 col-start-9',
          },
        ],
      },
    ],
  },
]
