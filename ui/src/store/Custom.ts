export default [
  /// OBS! Not using Tabs in correct way, se ServiceHMI for updated usage
  {
    component: 'Tabs',
    class: 'row-span-12 col-span-12',
    tabs: [
      {
        active: true,
        text: 'Parameters',
        content: [
          {
            name: 'Sensor temperature',
            opckey: 'Pattern_Edit.rod_offset',
            component: 'BaseBoolIndicator',
            color: 'green',
          },
          {
            name: 'PTL1200',
            opckey: 'Pattern_edit.numb4',
            component: 'BaseBoolIndicator',
            color: 'green',
          },
          {
            name: 'Burner Temp',
            opckey: 'Pattern_Edit.ext_pour_offset',
            component: 'BaseButton',
          },
        ],
      },
      {
        active: false,
        text: 'Service Parameters',
        content: [
          {
            name: 'PTL4100',
            opckey: 'Pattern_edit.numb5',
            component: 'BaseEditOpcNumber',
            columnSize: 3,
          },
          {
            name: 'PTL-B',
            opckey: 'Pattern_Edit.rod_offset',
            component: 'BaseOpcNumber',
            columnSize: 3,
          },
          {
            name: 'Furnes Pressure',
            opckey: 'Pattern_edit.numb8',
            component: 'BaseEditOpcNumber',
            columnSize: 3,
          },
        ],
      },
      {
        active: false,
        text: 'Diagnostics',
        content: [
          {
            name: 'Sensor temp',
            opckey: 'Pattern_edit.pour_time',
            component: 'BaseOpcNumber',
            columnSize: 3,
          },
          {
            name: 'PTL4100',
            opckey: 'Pattern_edit.numb5',
            component: 'BaseEditOpcNumber',
            columnSize: 3,
          },
          {
            name: 'PTL-B',
            opckey: 'Pattern_Edit.rod_offset',
            component: 'BaseOpcNumber',
            columnSize: 3,
          },
          {
            name: 'Burner Temp',
            opckey: 'Pattern_Edit.ext_pour_offset',
            component: 'BaseBoolIndicator',
            color: 'green',
          },
        ],
      },
    ],
  },
]
