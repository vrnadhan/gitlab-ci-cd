export default [
  {
    component: 'FurnaceDemo',
    class: 'row-start-3 col-start-1 row-span-6 col-span-6',
    opckey: 'FURPRE_Iron_Contents',
    max: 100,
    demo: true,
    id: 'svg',
  },
  {
    name: 'Pressure Enabled',
    component: 'BaseBoolIndicator',
    class: 'row-start-10 col-start-7',
    opckey: 'FURPRE_HMI_Enable_pressure',
    color: 'green',
  },
  {
    name: 'Setpoint [mbar]',
    class: 'row-start-9 col-start-5',
    opckey: 'FURPRE_Mbar_setpoint',
    component: 'BaseEditOpcNumber',
  },
  {
    name: 'Furnace Full',
    class: 'row-start-2 col-start-1',
    component: 'BaseBoolIndicator',
    opckey: 'FURPRE_Pressure_Full',
    textLeft: true,
    color: 'green',
  },
  {
    name: 'Furnace Refill',
    class: 'row-start-2 col-start-3',
    component: 'BaseBoolIndicator',
    opckey: 'FURPRE_Pressure_Refill',
    textLeft: true,
    color: 'green',
  },
  {
    name: 'Furnace Empty',
    class: 'row-start-2 col-start-5',
    component: 'BaseBoolIndicator',
    opckey: 'FURPRE_Pressure_Empty',
    textLeft: true,
    color: 'green',
  },
  {
    name: 'Crod1',
    class: 'row-start-4 col-start-7',
    component: 'BaseBoolIndicator',
    opckey: 'FURPRE_Pressure_Full4',
    color: 'green',
  },
  {
    name: 'Crod2',
    class: 'row-start-5 col-start-7',
    component: 'BaseBoolIndicator',
    opckey: 'FURPRE_Pressure_Refill3',
    color: 'green',
  },
  {
    name: 'Crod3',
    class: 'row-start-6 col-start-7',
    component: 'BaseBoolIndicator',
    opckey: 'FURPRE_Pressure_Empty2',
    color: 'red',
  },
  {
    name: 'Crod4',
    class: 'row-start-7 col-start-7',
    component: 'BaseBoolIndicator',
    opckey: 'FURPRE_Pressure_Empty1',
    color: 'red',
  },
  {
    name: 'Actual pressure [mbar]',
    class: 'row-start-10 col-start-5',
    opckey: 'FURPRE_Mbar_actual',
    component: 'BaseEditOpcNumber',
  },
]
// For iron visu SVG use, FURPRE_Iron_Contents

//  EmergencyRods Red1: FURPRE_em_rods Red2: FURPRE_em_rods
// Green1: FURPRE_Lo_rod Green2: FURPRE_Up_rod
