import { createStore } from 'vuex'
import OPCValueList from '@hmi/types/OPCValueList'
import WebSocketClient from '../WebsocketClient'
import FurnacePower from './FurnacePower'
import FurnacePressure from './FurnacePressure'
import ServiceHMI from './ServiceHMI'
import Custom from './Custom'
import Home from './Home'
import Laser from './Laser'
import Alarms from './Alarms'
import Pouring from './Pouring'
import menuButtons from './MenuButtons'
import serviceButtons from './ServiceButtons'
import bottomBarIndicators from './BottomBarIndicators'
import ServiceExample from './ServiceExample'
import EnumLists from './EnumLists'
import {
  getNodes,
  getNode,
  subscribeNode,
  unSubscribeNode,
  getPages,
} from '../Api'

const ws = new WebSocketClient()

export default createStore({
  state: {
    bottomBarIndicators,
    FurnacePower,
    FurnacePressure,
    ServiceHMI,
    Custom,
    Laser,
    Home,
    Alarms,
    Pouring,
    menuButtons,
    serviceButtons,
    ServiceExample,
    nodes: Array<any>(),
    datapoints: {} as OPCValueList,
    achievedLevel: {
      list: [],
      diff: {},
    },
    lineLaserLevel: [] as number[],
    sensorGraphMockData: [],
    activeView: Home,
    activeViewName: 'Home',
    alarmInView: '',
    pages: [],
    ...EnumLists,
  },
  mutations: {
    setOPCData(state, data) {
      if (data.type === 'AchievedLevel') {
        if (data.diff) {
          state.achievedLevel.diff = data.diff
        }
        if (data.list) {
          state.achievedLevel.list = data.list
        }
      }

      // else if (data.type === 'LineLaserGraph') {
      //   state.lineLaserLevel = data.list
      // }
      else {
        if (data.dataType === 10) {
          const value = Math.round(data.value * 100) / 100
          state.datapoints[data.key] = { ...data, value }
        } else {
          state.datapoints[data.key] = data
        }
      }
    },
    setNodes(state, nodes) {
      state.nodes = nodes
    },
    setNode(state, n: any) {
      const i = state.nodes.findIndex(({ node }) => n.node === node)
      state.nodes[i] = n
    },
    setAlarmInView(state, alarm) {
      state.alarmInView = alarm
    },
    viewHome(state) {
      state.activeView = state.Home
      state.activeViewName = 'Home'
    },
    viewFurnacePower(state) {
      state.activeView = state.FurnacePower
      state.activeViewName = 'FurnacePower'
    },
    viewFurnacePressure(state) {
      state.activeView = state.FurnacePressure
      state.activeViewName = 'FurnacePressure'
    },
    viewPouring(state) {
      state.activeView = state.Pouring
      state.activeViewName = 'Pouring'
    },
    viewAlarms(state) {
      state.activeView = state.Alarms
      state.activeViewName = 'Alarms'
    },
    viewCustom(state) {
      state.activeView = state.Custom
      state.activeViewName = 'Custom'
    },
    viewServiceHMI(state) {
      state.activeView = state.ServiceHMI
      state.activeViewName = 'ServiceHMI'
    },
    viewLaser(state) {
      state.activeView = state.Laser
      state.activeViewName = 'Laser'
    },
    viewServiceExample(state) {
      state.activeView = state.ServiceExample
      state.activeViewName = 'ServiceExample'
    },
    // addRow(state, { page, tab, id }) {
    //   const t = state.pages[page].tabs.find(p => p.name === tab)
    //   t.content.push({ id })
    // },
    setPages(state, pages) {
      state.pages = pages
    },
  },
  actions: {
    setOPCData({ commit }, data) {
      commit('setOPCData', data)
    },
    updateOPCData(store, data) {
      ws.send(data)
    },
    setAlarmInView({ commit }, alarm) {
      commit('setAlarmInView', alarm)
    },
    toggleKeyboard({ commit }) {
      commit('TOGGLE_KEYBOARD')
    },
    keyboardTriggerd({ commit, dispatch }, opcKey: string) {
      commit('SET_FOCUS_OPC_KEY', opcKey)
      dispatch('toggleKeyboard')
    },
    viewPanel({ commit }, view) {
      if (view === 'Home') {
        commit('viewHome')
      } else if (view === 'FurnacePower') {
        commit('viewFurnacePower')
      } else if (view === 'FurnacePressure') {
        commit('viewFurnacePressure')
      } else if (view === 'Pouring') {
        commit('viewPouring')
      } else if (view === 'Alarms') {
        commit('viewAlarms')
      } else if (view === 'Custom') {
        commit('viewCustom')
      } else if (view === 'ServiceHMI') {
        commit('viewServiceHMI')
      } else if (view === 'Laser') {
        commit('viewLaser')
      } else if (view === 'ServiceExample') {
        commit('viewServiceExample')
      }
    },
    async fetchNodes({ commit }) {
      const nodes = await getNodes()
      if (!nodes) {
        return
      }
      commit('setNodes', nodes)
    },
    async fetchPages({ commit }) {
      const pages = await getPages()
      if (!pages) {
        return
      }
      commit('setPages', pages)
    },
    async subscribeOPCNode({ commit }, node) {
      await subscribeNode(node)
      const n = await getNode(node)
      commit('setNode', n)
    },
    async unSubscribeOPCNode({ commit }, node) {
      await unSubscribeNode(node)
      const n = await getNode(node)
      commit('setNode', n)
    },
  },
  getters: {
    opc: (state) => (key: string) => {
      return state.datapoints && state.datapoints[key]
        ? state.datapoints[key]
        : { value: '', dataType: '' }
    },
    pourTime: (state) => {
      return state.datapoints && state.datapoints['Pattern_current.pour_time']
        ? state.datapoints['Pattern_current.pour_time']
        : ''
    },
    achievedLevel: (state) => {
      return state.achievedLevel
    },
    lineLaserLevel: (state) => {
      return state.lineLaserLevel
    },
    sensorGraphMockData: (state) => {
      return state.sensorGraphMockData
    },
    alarmInView: (state) => {
      return state.alarmInView
    },
  },
  modules: {},
})
