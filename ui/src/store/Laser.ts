export default [
  {
    component: 'GraphLineLaser',
    class: 'row-start-1 col-start-1 row-span-4 col-span-6',
  },
  // {
  //   component: 'GraphSensorImg',
  //   class: 'row-start-1 col-start-7 row-span-6 col-span-6',
  // },
  {
    name: 'Calibrate Spot',
    class: 'row-start-12 col-start-1',
    value: 40,
    columnSize: 2,
    component: 'ButtonCommand',
  },
  {
    name: 'Calibrate Line',
    class: 'row-start-12 col-start-4',
    value: 42,
    columnSize: 2,
    component: 'ButtonCommand',
  },
  {
    name: 'Level [mm]',
    opckey: 'SENS_Level_avg_mm',
    class: 'row-start-6 col-start-1',
    component: 'BaseEditOpcNumber',
  },
  {
    name: 'Level Avrage',
    class: 'row-start-6 col-start-3',
    opckey: 'SENS_Level_avg',
    component: 'BaseEditOpcNumber',
  },
  {
    name: 'Validity',
    class: 'row-start-6 col-start-5',
    opckey: 'SENS_Validity',
    component: 'BaseEditOpcNumber',
  },
  {
    name: 'Intensity',
    class: 'row-start-7 col-start-1',
    opckey: 'SENS_Intensity',
    component: 'BaseEditOpcNumber',
  },
  {
    name: 'Temp Laser',
    class: 'row-start-7 col-start-3',
    opckey: 'SENS_Temp_Laser',
    component: 'BaseEditOpcNumber',
  },
  {
    name: 'Temp Sensor',
    class: 'row-start-7 col-start-5',
    opckey: 'SENS_Temp_Sensor',
    component: 'BaseEditOpcNumber',
  },
  {
    name: 'Cup Depth',
    class: 'row-start-8 col-start-2',
    opckey: 'Par.SENS_Cup_depth',
    component: 'BaseEditOpcNumber',
  },
  {
    name: 'Cup Trig',
    class: 'row-start-8 col-start-4',
    opckey: 'Par.SENS_Cup_trig',
    component: 'BaseEditOpcNumber',
  },
]
