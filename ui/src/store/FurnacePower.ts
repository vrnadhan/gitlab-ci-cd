export default [
  // SVG
  {
    component: 'LadleDemo',
    class: 'row-start-1 col-start-1 row-span-5 col-span-5',
    opckey: 'FURPRE_Iron_Contents',
    max: 100,
    demo: true,
    id: 'svg',
  },

  // Main indicators
  {
    name: 'High power timer [min]',
    class: 'row-start-6 col-start-1',
    opckey: 'Par.FURPOW_VB1_High_Timer',
    component: 'BaseEditOpcNumber',
    columnSize: 2,
  },
  {
    name: 'Low power timer [min]',
    opckey: 'Par.FURPOW_VB1_Low_Timer',
    class: 'row-start-6 col-start-3',
    component: 'BaseEditOpcNumber',
    columnSize: 2,
  },
  {
    name: 'Pause timer [min]',
    class: 'row-start-7 col-start-2',
    opckey: 'Par.FURPOW_VB1_Pause_Timer',
    component: 'BaseEditOpcNumber',
    columnSize: 2,
  },
  {
    name: 'Remaining time [sec]',
    class: 'row-start-8 col-start-2',
    opckey: 'FURPOW_Remaining_Time',
    component: 'BaseEditOpcNumber',
  },
  // end Main indicators

  // inductorIndicators
  {
    name: 'Inductor power [kW]',
    class: 'row-start-1 col-start-10',
    component: 'BaseOpcNumber',
    opckey: 'FURPOW_Furnace_Inductor_Power',
  },
  {
    name: 'Inductor Voltage [V]',
    class: 'row-start-3 col-start-10',
    component: 'BaseOpcNumber',
    opckey: 'FURPOW_Furnace_Inductor_Voltage',
  },
  {
    name: 'Inductor Current [A]',
    class: 'row-start-5 col-start-10',
    component: 'BaseOpcNumber',
    opckey: 'FURPOW_Furnace_Inductor_Current',
  },
  {
    name: 'Reactive Power [kVar]',
    class: 'row-start-7 col-start-10',
    component: 'BaseOpcNumber',
    opckey: 'FURPOW_Furnace_Inductor_Reactive_Power',
  },
  // end inductorIndicators

  // middleSection
  // {
  //   name: 'Spout Temp',
  //   component: 'termometer',
  //   opckey: 'EASYPOUR_initializing_slider',
  //   max: 100,
  //   columnSize: 4,
  // },
  // {
  //   name: 'Stream Temp',
  //   component: 'termometer',
  //   opckey: 'EASYPOUR_initializing_slider2',
  //   max: 100,
  //   columnSize: 4,
  // },
  {
    name: 'Power modes',
    class: 'row-start-6 col-start-6',
    component: 'modalSelector',
    opckey: 'FURPOW_Mode',
    selections: [
      'Off',
      'Manual',
      'Auto',
      'Superheat',
      'Sinter',
      'Calibration',
      'Weartrend',
      'Current Compensation',
      'Auto timer',
      'Auto Temperatur',
    ],
  },
  {
    name: 'Cooling Status',
    class: 'row-start-1 col-start-7',
    component: 'BaseBoolIndicator',
    opckey: 'FURCOW_Status_OK',
    color: 'red',
  },
  {
    name: 'Superheat',
    class: 'row-start-2 col-start-7',
    component: 'BaseBoolIndicator',
    opckey: 'FURPOW_Power_Step_3',
    color: 'green',
  },
  {
    name: 'High Power',
    class: 'row-start-3 col-start-7',
    component: 'BaseBoolIndicator',
    opckey: 'FURPOW_Power_Step_2',
    color: 'green',
  },
  {
    name: 'Low Power',
    class: 'row-start-4 col-start-7',
    component: 'BaseBoolIndicator',
    opckey: 'FURPOW_Power_Step_1',
    color: 'green',
  },
  // end Middle section
]
