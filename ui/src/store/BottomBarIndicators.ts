export default [
  {
    nameRef: 'RodClosePosition',
    opckey: 'ACT_Close_pos',
    order: 1,
  },
  {
    name: 'Fade Timer',
    nameRef: 'FadeTimer',
    opckey: 'POUR_FADE_current_Fade_Time_Sec',
    order: 2,
  },
  {
    nameRef: 'CycleTime',
    opckey: 'MON_Cycletime',
    order: 3,
  },
  {
    nameRef: 'CoolingTimer',
    opckey: 'CTC_Cooling_timer',
    order: 4,
  },
  {
    nameRef: 'MoldCounter',
    opckey: 'HMI_Mold_Counter',
    order: 5,
  },
  {
    nameRef: 'ActualPourTime',
    opckey: 'POUR_Actual_pour_time',
    order: 6,
  },
]
