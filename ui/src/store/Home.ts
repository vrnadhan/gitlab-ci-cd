export default [
  {
    component: 'FurnaceDemo',
    class: 'row-start-13 col-start-7 row-span-5 col-span-6',
  },
  {
    component: 'LadleDemo',
    class: 'row-start-13 row-span-5 col-span-5',
  },
]
