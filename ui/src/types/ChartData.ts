import RGBColor from "@/RGBColor"
import { reactive } from "vue";

interface DataList {
    list: number[]
}

export default class ChartData {
    data: DataList
    labels: DataList
    color: RGBColor

    constructor(list: number[], color?: RGBColor) {
        this.data = list ? reactive({ list }) : reactive({ list: [] })
        this.color = color ? color : new RGBColor(234, 33, 44)
        this.labels = reactive({ list: [] })
    }

    generateLabels(): number[] {
        console.log(this.data.list.length)
        return new Array(Math.ceil(this.data.list.length)).fill(0)
    }

    setData(list: number[]) {
        this.data.list = list
        this.labels.list = this.generateLabels()
    }
}