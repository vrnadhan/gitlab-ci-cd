import {Ref} from 'vue'

type GraphList = {list:number[]}

export default interface LineLaseGraph {
    update(list:number[]): void
    state: GraphList
}