import RGBColor from './RGBColor'

interface LineLaserState {
  list: number[]
}

export default class Dataset {
  data: number[] | LineLaserState
  color: RGBColor
  pointSize?: number
  noLine?: boolean

  constructor(
    data: number[],
    color: RGBColor,
    pointSize?: number,
    noLine?: boolean
  ) {
    this.data = data
    this.color = color
    this.pointSize = pointSize ? pointSize : 0
    this.noLine = noLine
  }
}
