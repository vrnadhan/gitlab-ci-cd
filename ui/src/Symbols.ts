import { InjectionKey, Ref } from 'vue';
import ChartData from './types/ChartData';
import LineLaseGraph from './types/LineLaser';

const LineLaserStateKey: InjectionKey<ChartData> = Symbol('LineLaseState');
const AchiveiedLevelStateKey: InjectionKey<ChartData> = Symbol('AchiveiedLevelState');

export { LineLaserStateKey }
