import { createApp, Component } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { library } from '@fortawesome/fontawesome-svg-core'
import {
  faLock,
  faRedo,
  faLockOpen,
  faSave,
  faSignOutAlt,
  faFolderOpen,
  faPlus,
  faMinus,
  faPlay,
  faStop,
  faThumbsUp,
  faThumbsDown,
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import './assets/css/tailwind.css'
import upperFirst from 'lodash/upperFirst'
import camelCase from 'lodash/camelCase'
import LineLaserState from './state/LineLaser'
import { LineLaserStateKey } from './Symbols'

library.add(faLock)
library.add(faPlay)
library.add(faStop)
library.add(faThumbsUp)
library.add(faThumbsDown)
library.add(faPlus)
library.add(faMinus)
library.add(faRedo)
library.add(faSave)
library.add(faLockOpen)
library.add(faSignOutAlt)
library.add(faFolderOpen)

const app = createApp((App as unknown) as Component)
  .use(store)
  .use(router)
  .component('font-awesome-icon', FontAwesomeIcon)

app.provide(LineLaserStateKey, LineLaserState)

const requireComponent = require.context(
  './components/base',
  false,
  /Base[A-Z]\w+\.(vue|js)$/
)

requireComponent.keys().forEach((fileName: string) => {
  const componentConfig = requireComponent(fileName)

  const componentName = upperFirst(
    camelCase(fileName.replace(/^\.\/(.*)\.\w+$/, '$1'))
  )

  app.component(componentName, componentConfig.default || componentConfig)
})

app.mount('#app')
