import Vector from './Vector'

export default class IntervalList {
  list: Array<Vector>
  constructor(list?: Array<Vector>) {
    this.list = list ? list : []
  }

  push(i: Vector) {
    this.list.push(i)
  }

  length(): number {
    return this.list.length
  }

  get(i: number): Vector {
    return this.list[i]
  }
}
