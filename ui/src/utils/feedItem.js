export default function feedcomponent(component) {
  let propObject = {}
  if (component === 'BoolIndicator') {
    propObject = {
      class: component.class,
      name: component.name,
      opckey: component.key,
      color: component.color,
      style: component.style,
    }
  } else if (component === 'serviceopclist') {
    propObject = {
      class: component.class,
      opclist: component.opclist,
    }
  } else if (component === 'BaseEditOpcNumber') {
    propObject = {
      nameRef: 'noNameIndicator',
      opckey: 'Pattern_Edit.pour_time',
      columnSize: 8,
      rowSize: 4,
      labelPosition: 'label-top',
      doneButtonText: 'Save',
      conditions: { opckey: '', values: [] },
    }
  } else if (component === 'BaseOpcNumber') {
    propObject = {
      class: component.class,
      name: component.name,
      opckey: component.key,
      columnSize: component.columnSize,
      rowSize: component.rowSize,
    }
  } else if (component === 'Furnace') {
    propObject = {
      class: 'row-span-5 col-span-6',
      opckey: 'FURPRE_Iron_Contents',
      mainValue: 0,
      nozzleValue: 0,
      stopperPos: 0,
      max: 100,
      size: 16,
      id: 'svg',
    }
  } else if (component === 'LadleDemo') {
    propObject = {
      class: component.class,
      opckey: 'FURPRE_Iron_Contents',
      max: 100,
      demo: true,
      id: 'svg',
    }
  } else if (component === 'div') {
    propObject = {
      class: component.class,
    }
  } else if (component === 'BaseEditOpcString') {
    propObject = {
      class: component.class,
      name: component.name,
      opckey: component.key,
      columnSize: component.columnSize,
      id: component.id,
    }
  } else if (component === 'BaseButton') {
    propObject = {
      class: component.class,
      name: component.name,
      opckey: component.key,
    }
  } else if (component === 'ButtonCommand') {
    propObject = {
      class: component.class,
      name: component.name,
      value: component.value,
      columnSize: component.columnSize,
    }
  } else if (component === 'GraphLineLaser') {
    propObject = {
      class: component.class,
    }
  } else if (component === 'GraphSensorImg') {
    propObject = {
      class: component.class,
    }
  } else if (component === 'BasePanelContainer') {
    propObject = {
      class: component.class,
      columnSize: component.columnSize,
      rowSize: component.rowSize,
      flow: component.flow,
      content: component.content,
    }
  } else if (component === 'BasePanel') {
    propObject = {
      class: component.class,
      columnSize: component.columnSize,
      rowSize: component.rowSize,
      content: component.content,
    }
  } else if (component === 'patternControl') {
    propObject = {
      class: component.class,
      columnSize: component.columnSize,
    }
  } else if (component === 'pouringCanvas') {
    propObject = {
      class: component.class,
    }
  } else if (component === 'RodPositionChart') {
    propObject = {
      class: component.class,
    }
  } else if (component === 'modalSelector') {
    propObject = {
      class: component.class,
      name: component.name,
      opckey: component.key,
      selections: component.selections,
    }
  } else if (component === 'tableSelect') {
    propObject = {
      class: component.class,
      name: component.name,
      tableColumns: component.tableColumns,
      tableContent: component.tableContent,
    }
  } else if (component === 'alarmInfo') {
    propObject = {
      class: component.class,
      tableColumns: component.tableColumns,
    }
  } else if (component === 'FurnaceDemo') {
    propObject = {
      class: component.class,
      opckey: 'FURPRE_Iron_Contents',
      max: 100,
      id: 'svg1',
    }
  } else if (component === 'Tabs') {
    propObject = {
      class: component.class,
      tabs: component.tabs,
    }
  } else if (component === 'AlarmsOldList') {
    propObject = {
      class: component.class,
    }
  } else if (component === 'AlarmsCurrentList') {
    propObject = {
      class: component.class,
    }
  }

  return propObject
}
