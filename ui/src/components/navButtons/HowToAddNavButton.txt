1. Import svg icon to Affinity Designer
2. Group layers: One main group with button-name that has a "content group" and a "background"
3. Store plain svg file in src/assets/navButtons
4. Create vue component for the button
5. Copy-Paste code from older navButton component
6. Copy-Paste new svg-code into component and remove <?xml> and <!DOCTYPE>

7.  Set these attributes in <svg>
    
    width="93%"
    height="93%"
    class="block m-auto"
    viewBox="0 0 2009 2009"

8. Add 
      v-bind:style="{ fill: serviceMode ? '#ffb000' : '#3d4045' }"
   to background <path>

9. Add class"pressdown" to "content" <g>