module.exports = {
  purge: {
    enabled: false,
    content: ['./src/**/*.vue'],
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        main: '#27292D',
        infolist: '#191D1F',
        infowindow: '#2D3034',
        white: '#FFFFFF',
        black: '#000000',
        red: '#C90D2C',
        yellow: '#EED409',
        green: '#2DAE31',
        button: '#8e8e8e',
        buttonpressed: '#4a4a4a',
        buttonframe: '#3d4045',
        highlight: '#FF8200',
        serviceframe: '#ffb000',
      },
    },
  },
  variants: {
    extend: {
      opacity: ['disabled'],
      backgroundColor: ['active', 'hover'],
    },
  },
  plugins: [require('@tailwindcss/forms')],
}
