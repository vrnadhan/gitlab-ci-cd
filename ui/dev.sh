cp -r ../types src/types && \
docker build . -t hmi-ui-3 && \
docker run \
  -e CHOKIDAR_USEPOLLING=true \
  --rm \
  -p 8080:8080 \
  -v "$PWD/src":/app/src \
  --tty \
  --name hmi-ui-3 \
  -it \
  hmi-ui-3 \
  yarn serve
